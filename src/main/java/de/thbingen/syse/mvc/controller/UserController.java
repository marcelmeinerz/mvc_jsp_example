package de.thbingen.syse.mvc.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import de.thbingen.syse.mvc.data.User;
import de.thbingen.syse.mvc.propertybean.PropertyBean;
import de.thbingen.syse.mvc.repos.RepositoryService;

@Controller
public class UserController {
	@Autowired
	RepositoryService repos;

	@Autowired
	PropertyBean propertyBean;

	@RequestMapping(value = "/users", method = RequestMethod.GET)
	public String deleteUser(@RequestParam int id, Map<String, Object> model) {

		if (repos.getUsers().findById(id) != null) {
			User user = repos.getUsers().findById(id);
			repos.getUsers().delete(user);
		}

		model.put("userList", repos.getUsers().findAll());
		model.put("nextJSPTable", "tableuser.jsp");
		return "index";
	}

	@PostMapping("/users")
	public String createUser(@ModelAttribute("addUser") User user, Map<String, Object> model) {
		repos.getUsers().save(user);
		model.put("userList", repos.getUsers().findAll());
		model.put("nextJSPTable", "tableuser.jsp");
		return "index";
	}

	@RequestMapping(value = "/loadUser", method = RequestMethod.GET)
	public String user(Map<String, Object> model) {

		model.put("userList", repos.getUsers().findAll());
		model.put("nextJSPTable", "tableuser.jsp");
		return "index";
	}
}
