package de.thbingen.syse.mvc.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class BaseController {

	// for testing injection and model-view methods
	// inject via application.properties
	@Value("${welcome.message:test}")
	private String message = "Reading the value from the property file failed.";
	
	@RequestMapping(value = "/error", method = RequestMethod.GET)
	public String error(Map<String,Object> model) {
		model.put("message", "");
		return "index";
	}
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String login(Map<String,Object> model) {
		model.put("message", "");
		return "index";
	}

	

	
}
