package de.thbingen.syse.mvc.data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class User implements MyEntity {

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private int id;
	String name;
	String password;
	String role;
	
	public User() {
		// TODO Auto-generated constructor stub
	}
	
	public User(String name, String password, String role) {
		this.name = name;
		this.password = password;
		this.role = role;
	}

	@OneToOne(mappedBy = "product_id")
	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getPassword() {
		return password;
	}

	public String getRole() {
		return role;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPassword(String password) {
		this.password = password;
	}

    public void setRole(String role) {
		this.role = role;
	}
	
	
	
}
