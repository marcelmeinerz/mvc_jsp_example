package de.thbingen.syse.mvc.propertybean;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class PropertyBean {
	
	
	@Value("${welcome.message:test}")
	private String message = "Reading the value from the property file failed.";
	
	@Value("${error.message:test}")
	private String error = "Reading the value from the property file failed.";
	
	public PropertyBean() { }
	
	
	public String getError() {
		return error;
	}
	
	public String getMessage() {
		return message;
	}
	

}
