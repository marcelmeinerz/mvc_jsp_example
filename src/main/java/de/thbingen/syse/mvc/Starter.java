package de.thbingen.syse.mvc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.util.Assert;

@SpringBootApplication (scanBasePackages = "de.thbingen.syse.mvc")
public class Starter {

	public static void main(String[] args) {
		
		ApplicationContext ctx = SpringApplication.run(Starter.class, args);
	    Assert.notNull(ctx,"Could not start application context."); 
	
	}
}
