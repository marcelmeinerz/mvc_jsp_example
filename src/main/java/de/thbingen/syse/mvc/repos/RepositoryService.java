package de.thbingen.syse.mvc.repos;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RepositoryService {

	
	@Autowired
	UserRepository users;
	
	public RepositoryService() {	}

	public UserRepository getUsers() {
		return users;
	}

	public void setUsers(UserRepository users) {
		this.users = users;
	}
}
