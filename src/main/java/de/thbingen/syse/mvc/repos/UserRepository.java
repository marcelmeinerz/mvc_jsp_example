package de.thbingen.syse.mvc.repos;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import de.thbingen.syse.mvc.data.User;



@Repository
public interface UserRepository extends CrudRepository<User, Long>{

	public User findById(@Param("id") Integer id);
	
	public User findByName(@Param("name") String name);
}
