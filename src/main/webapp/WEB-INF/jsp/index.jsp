<%-- 
    Document   : welcome
    Created on : 13.11.2017, 20:14:44
    Author     : Marcel <marcel.meinerz@th-bingen.de>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	String myVariable = "";
%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- <link rel="stylesheet" type="text/css" href="style.css">  -->

<c:url value="/css/style.css" var="jstlCss" />
<link href="${jstlCss}" rel="stylesheet" />


<title>Welcome Page</title>
</head>
<body>
	<!-- Beginn Logo -->
	<div id="logo">
		<!-- Beginn horizontale Navigation -->
		<div id="navi">
			<a href="/">Home</a> <a href="#">Settings</a> <a href="#">Logout</a>
		</div>
		<!-- Ende horizontale Navigation -->
	</div>
	<!-- Ende Logo -->
	<!-- Beginn Rahmen für Tabelle -->
	<div id="rahmen">
		<!-- Beginn zentrierte Tabelle -->
		
			<table id="content" cellpadding="0" cellspacing="0">
				<tr>
					<td valign="top" id="spalte_li">
						<!-- Beginn linke Tabellenspalte --> <!-- vorformatiertes Beispiel-Suchformular -->
						<form action="#">
							<input name="suche" class="suchbox" size="20" type="text"
								onFocus="this.value = ''" value="Suche"> 
							<input title="Suche starten" size="2" name="send" class="suchbutton"
								value="Go" type="submit">
						</form> <!-- Ende des Beispiel-Suchformulares --> <!-- Beginn vertikale Navigation -->
						<div class="navi_vert">
							<span>ESL-Tool</span> 
							
							<a href="/loadUser"  onclick=>User</a>
						</div> <!-- Ende vertikale Navigation -->
					</td>
					<!-- Ende linke Tabellenspalte -->
					<td valign="top" id="inhalt" >
						<!-- Beginn Inhaltsspalte -->
						<h1>Welcome</h1> 
  							<jsp:include page="${nextJSPTable}" flush="true" />
  							
  						<!-- Ende Inhaltsspalte -->
					</td>
				</tr>
			</table>
		
		<!-- Ende zentrierte Tabelle -->
	</div>
	<!-- Ende Rahmen für Tabelle -->
	<div id="fuss">
		<!-- Beginn Fußzeile -->
		&#169; 2008 Ihr Name |
		<!-- Beginn Backlink zu www.homepage-vorlagen-webdesign.de -->
		Designed by <a href="link" target="_blank" title="Site">Site</a>
		<!-- Ende Backlink - Der Backlink darf nur bei Erwerb der Vorlage entfernt werden! -->
		| <a href="#">RSS Feed</a> | <a target="_blank"
			href="http://validator.w3.org/check?uri=http%3A%2F%2Fwww.homepage-vorlagen-webdesign.de%2Fhomepagevorlagen%2Fhomepage-vorlage-12%2Findex.htm">Valid
			HTML</a> | <a target="_blank"
			href="http://jigsaw.w3.org/css-validator/validator?uri=http://www.homepage-vorlagen-webdesign.de/homepagevorlagen/homepage-vorlage-12/css/style.css">Valid
			CSS</a>
		<!-- Ende Fußzeile -->
	</div>
</body>
</html>
