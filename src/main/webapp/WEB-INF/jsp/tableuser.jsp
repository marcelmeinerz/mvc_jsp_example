<%@page import="java.util.ArrayList"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<link rel="stylesheet" type="text/css" href="/css/tables.css">
<table class="style_table" id="user_table">
	<caption>
		<h3>User</h3>
	</caption>
	<thead>
		<tr>
			<td>ID</td>
			<td>Name</td>
			<td>Rolle</td>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${userList}" var="temp">
			<tr>
				<td class="userID" width="20%">${temp.id}</td>
				<td class="userName" width="35%">${temp.name}</td>
				<td class="userRole" width="35%">${temp.role}</td>
				<td width="10%">
				 <a	onclick="return confirm('Are you sure you want to delete?')"
					href="/users?id=${temp.id}" > <img alt="Del"
						src="./delete.png" width="28" height="28"></a>
				</td>
			</tr>
		</c:forEach>
	</tbody>
</table>
<form:form action="/users" method="post" modelAttribute="addUser">
	<p>${createMessage}</p>
	<input id="add" type="button" value="Add new User">
	<dialog id="dialog">
	<h3>Anlegen</h3>
	<table>
		<tr>
			<td><label for="name">Bitte geben Sie den Name an: </label></td>
			<td><input id="name" name="name" type="text" /><br /></td>
		</tr>
		<tr>
			<td><label for="pass">Bitte geben Sie das Passwort an:</label></td>
			<td><input id="pass" name="pass" type="password" /><br /></td>
		</tr>
		<tr>
			<td><label for="pass2">Bitte wiederholen Sie das
					Passwort an: </label></td>
			<td><input id="pass2" type="password" /><br /></td>
		</tr>
		<tr>
			<td><label for="role">Bitte wählen Sie die Rolle: </label></td>
			<td><select name="role">
					<option value="Admin">Admin</option>
					<option value="SuperUser">SuperUser</option>
					<option value="User">User</option>
			</select> <br /></td>
		</tr>
		<tr>
			<td colspan="2" align="right"><input type="submit" id="Anlegen"
				value="Anlegen" />
				<input type="button" id="cancelAdd" value ="Abbrechen"/></td>
		</tr>
	</table>
	</dialog>
</form:form>



<script type="text/javascript">
		
		
		var addUser = document.getElementById("add");
		var addDialog = document.getElementById('dialog');
		var add = document.getElementById("Anlegen");
		var cancel = document.getElementById("Abbruch");
		addUser.addEventListener('click', zeigeAddFenster);
		add.addEventListener('click', schließeAddFenster);
		cancel.addEventListener('click', schließeAddFenster);

		function zeigeAddFenster() {
			addDialog.showModal();
		}

		function schließeAddFenster() {
			addDialog.close();
		}

	</script>
